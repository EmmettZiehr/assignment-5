#pragma once

#include <iostream>
#include <conio.h>
#include <string>

class TicTacToe
{
private:
	//Field Members.
	char m_board[9];
	int m_turnNum;
	int m_numTurns;
	char m_playerTurn;

public:
	//Constructor.
	TicTacToe()
	{
		SetTurnNum(0);
		SetNumTurns(9);
		SetPlayerTurn(PlayerTurn());
		for (int i = 0; i < 9; i++)
		{
			m_board[i]++;
			m_board[i] = ' ';
		}
	}
	//Mutators.
	void SetPlayerTurn(char playerTurn) { m_playerTurn = playerTurn; }
	void SetTurnNum(char turnNum) { m_turnNum = turnNum; }
	void SetNumTurns(char numTurns) { m_numTurns = numTurns; }
	//void SetBoard(char board) { m_board = board; }
	//Accessors.
	char GetPlayerTurn() { return m_playerTurn; }
	//OtherMethods.
	void DisplayBoard()
	{
		std::cout << m_board[0] << "|" << m_board[1] << "|" << m_board[2] << std::endl
			<< "-" << "|" << "-" << "|" << "-" << std::endl
			<< m_board[3] << "|" << m_board[4] << "|" << m_board[5] << std::endl
			<< "-" << "|" << "-" << "|" << "-" << std::endl
			<< m_board[6] << "|" << m_board[7] << "|" << m_board[8] << std::endl;
	}
	char PlayerTurn()
	{

		if (m_turnNum == 0 || m_turnNum == 2 || m_turnNum == 4 || m_turnNum == 6 || m_turnNum == 8) { m_playerTurn = 'X'; return m_playerTurn; }
		else { m_playerTurn = 'O'; return m_playerTurn; }
	}
	void Move(int position)
	{

		if (position == 1) { m_board[0] = PlayerTurn(); }
		if (position == 2) { m_board[1] = PlayerTurn(); }
		if (position == 3) { m_board[2] = PlayerTurn(); }
		if (position == 4) { m_board[3] = PlayerTurn(); }
		if (position == 5) { m_board[4] = PlayerTurn(); }
		if (position == 6) { m_board[5] = PlayerTurn(); }
		if (position == 7) { m_board[6] = PlayerTurn(); }
		if (position == 8) { m_board[7] = PlayerTurn(); }
		if (position == 9) { m_board[8] = PlayerTurn(); }

		m_turnNum++;
	}
	bool IsValidMove(int position)
	{
		if (position == 1) { if (m_board[0] == 'X' || m_board[0] == 'O') { return false; } }
		if (position == 2) { if (m_board[1] == 'X' || m_board[1] == 'O') { return false; } }
		if (position == 3) { if (m_board[2] == 'X' || m_board[2] == 'O') { return false; } }
		if (position == 4) { if (m_board[3] == 'X' || m_board[3] == 'O') { return false; } }
		if (position == 5) { if (m_board[4] == 'X' || m_board[4] == 'O') { return false; } }
		if (position == 6) { if (m_board[5] == 'X' || m_board[5] == 'O') { return false; } }
		if (position == 7) { if (m_board[6] == 'X' || m_board[6] == 'O') { return false; } }
		if (position == 8) { if (m_board[7] == 'X' || m_board[7] == 'O') { return false; } }
		if (position == 9) { if (m_board[8] == 'X' || m_board[8] == 'O') { return false; } }
		else { return true; }
	}
	bool win(char player)
	{
		// Horizontal wins.
		if (m_board[0] == player && m_board[1] == player && m_board[2] == player) { return true; }
		if (m_board[3] == player && m_board[4] == player && m_board[5] == player) { return true; }
		if (m_board[6] == player && m_board[7] == player && m_board[8] == player) { return true; }
		// Vertical wins.
		if (m_board[0] == player && m_board[3] == player && m_board[6] == player) { return true; }
		if (m_board[1] == player && m_board[4] == player && m_board[7] == player) { return true; }
		if (m_board[2] == player && m_board[5] == player && m_board[8] == player) { return true; }
		// Diagonal wins.
		if (m_board[0] == player && m_board[4] == player && m_board[8] == player) { return true; }
		if (m_board[2] == player && m_board[4] == player && m_board[6] == player) { return true; }
		else { return false; }
	}
	bool IsOver()
	{
		if (m_turnNum == 9) { return true; } // Ends game if tie.
		if (win('X')) { return true; } // Ends game if player wins.
		if (win('O')) { return true; } // Ends game if player wins.
		else { return false; }
	}
	void DisplayResult()
	{
		if (win('X') || win('O')) { std::cout << std::endl << GetPlayerTurn() << " wins!" << std::endl << std::endl; }
		else { std::cout << std::endl << "The game is a tie." << std::endl << std::endl; }
	}
};
